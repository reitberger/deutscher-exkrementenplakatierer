#include "util.h"
#include <stdint.h>
#define ARRLEN(x) sizeof (x) / sizeof (*x)


struct CPlayer player;
void
setup (void)
{
	//CPlayerWithNotes ((char **) &all_vor, ARRLEN(all_vor), &player);
	pinMode (PIN_MUSIC, OUTPUT);
}

void
loop (void)
{
	uint16_t Frequenz;
        for (uint8_t i = 0; i < 72; i++) {                        
            int Note_Nr = Melodie [i][0];                 // Nummer der Note
            if (Note_Nr == 99) { Frequenz = 0; }          // Note 99 = Pause
            else { Frequenz = Note [Note_Nr]; }           // Frequenz bestimmen
            int Zeit = Melodie [i][1] * 15;  // Tempo bestimmen
            tone (PIN_MUSIC, Frequenz, Zeit);                // Ton erzeugen
            delay(Zeit);                                  // Wartezeit
	    }
	    CPlayNotes ( sun, ARRLEN (sun), &player);
}