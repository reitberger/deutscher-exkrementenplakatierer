#include <stdint.h>
#ifndef UTIL_PLAKAT
#define UTIL_PLAKAT
#define PIN_MUSIC 7

const float BN = 1600.0, CN = BN / 2.0, CL = CN * 1.5, DN = CN / 2.0, DL = DN * 1.5, EN = CN / 2.0;
float w_notes[] = {16.35, 18.35, 20.60, 21.83, 24.50, 27.50, 30.87, 32.70, 36.71, 41.20, 43.65, 49.00, 55.00, 61.74, 65.41, 73.42, 82.41, 87.31, 98.00, 110.00, 123.47, 130.81, 146.83, 164.81, 174.61, 196.00, 220.00, 246.94, 261.63, 293.66, 329.63, 349.23, 392.00, 440.00, 493.88, 523.25, 587.33, 659.25, 698.46, 783.99, 880.00, 987.77, 1046.50};
float h_notes[] = {17.32, 19.45, 23.12, 25.96, 29.14, 34.65, 38.89, 46.25, 51.91, 58.27, 69.30, 77.78, 92.50, 103.83, 116.54, 138.59, 155.56, 185.00, 207.65, 233.08, 277.18, 311.13, 369.99, 415.30, 466.16, 554.37, 622.25, 739.99, 932.33};

(+ (* 4 5) (- ?e ?c))
float stf (char *str)
{
  uint8_t inter = str[2] == '#' ? 1 : str[2] == 'b' ? 2 : 0;
  float *notes = w_notes;
  int32_t i = 0;
  if (!inter)
    {
      i = *str == 'a' ? 5 : *str == 'h' ? 6 : *str - 'c';
      i = (str[1] - '0') * 7 + i;
    }
  else
    {
      i = *str == 'a' ? 4 : *str == 'h' ? 4 : *str - 'c';
      i = (str[1] - '0') * 5 + i;
      notes = h_notes;

    }
  return notes[i];
}

typedef struct CNote {
	char *note;
	float dur;
} CNote;

typedef struct CPlayer{
  char **exceptions;
  uint8_t exceptions_n;
} CPlayer;

void
CPlayerWithNotes (char ** notes, uint16_t notes_n, struct CPlayer *player)
{
	player->exceptions = notes;
	player->exceptions_n = notes_n;
}


void
CPlayNotes (struct CNote *notes, uint32_t notes_n, struct CPlayer *cplay)
{
	for (uint32_t i = 0; i < notes_n; i += 1)
	  {
	    float freq = notes[i].note[0] == 'p' ? 0 : stf (notes[i].note, *cplay->exceptions, cplay->exceptions_n);
	    tone (PIN_MUSIC, freq, notes[i].dur);
	    delay (notes[i].dur);
	  }
}

struct CNote rick[] ={
{"g4", CL },{"a4 ", CL },{"d4 ", CN },{"a4 ", CL },{"h4 ", CL },{"d5 ", EN },{"c5", EN },{"h4", DN },{"g4", CL },{"a4", CL },{"d4", CN },{"d4", CL },{"d4", EN },{"d4", EN },{"e4", EN },{"g4", DN },{"g4", EN },{"e4", DN },{"f4#", DN },{"g4", DN },{"g4", DN },{"a4", DN },{"f4#", DN },{"f4#", EN },{"e4", EN },{"d4", DN }, {"d4", BN}, 
{"e4", DN },{"e4", DN },{"f4#", DN },{"g4", DN },{"e4", CN },{"d4", DN },{"d5", DN }, {"p", EN}, {"d5", DN },{"a4", CL },  {"p", DN},
{"e4", DN },{"e4", DN },{"f4#", DN },{"g4", DN },{"e4", DN },{"g4", DN },{"a4", DN },{"p", EN}, {"f4#", DN },{"e4", DN },{"d4", CL }, {"p", DN},
{"e4", DN },{"e4", DN },{"f4#", DN },{"g4", DN },{"e4", DN },{"d4", CN },{"a4", DN },{"a4", DN },{"a4", DN },{"h4", DN },{"a4", CN }, {"p", DN},
{"g4", BN },{"g4", DN },{"a4", DN },{"h4", DN },{"g4", DN },{"a4", DN },{"a4", DN },{"a4", DN },{"h4", DN },{"a4", CN },{"d4", CN }, {"p", BN} ,{"e4", DN },{"f4#", DN },{"g4", DN },{"e4", DN },
{"p", EN}, {"a4", DN},{"h4 ", DN},{"a4", CL},{"d4 ", EN},{"e4 ", EN},{"g4 ", EN},{"e4 ", EN},{"h4 ", DL},{"h4 ", DL},{"a4 ", CL},{"d4 ", EN},{"e4 ", EN},{"g4 ", EN},{"e4 ", EN},{"a4 ", DL},{"a4 ", DL},{"g4 ", DL},{"f4# ", EN},{"e4 ", DN},{"d4 ", EN},{"e4 ", EN},{"g4 ", EN},{"e4 ", EN}, 
{"g4", CN},{"a4 ", DN},{"f4# ", DL},{"e4 ", EN},{"d4 ", DN},{"d4 ", DN},{"d4 ", DN},{"a4 ", CN},{"g4 ", BN},{"d4 ", EN},{"e4 ", EN},{"g4 ", EN},{"e4 ", EN}, 
{"h4 ", DL},{"h4 ", DL},{"a4 ", CL},{"d4 ", EN},{"e4 ", EN},{"g4 ", EN},{"e4 ", EN},{"d5 ", CN},{"f4# ", DN},{"g4 ", DL},{"f4# ", EN},{"e4 ", DN},{"d4 ", EN},{"e4 ", EN},{"g4 ", EN},{"e4 ", EN},{"g4 ", CN},{"a4 ", DN},{"f4# ", DL},{"e4 ", EN},{"d4 ", CN},{"d4 ", DN},{"a4 ", CN},{"g4 ", BN}, 
};

//h4 h4 a4 a4 g4 g4 h4 h4 a4 a4 g4 g4 h4 h4 a4 a4 g4 g4 h4 h4 h4 a4 a4 a4 a4 h4 h4
struct CNote ball[] = {
  {"h4", DN},{"h4 ", DN},{"h4 ", DN},{"c4 ", DL},{"h4 ", EN},{"a4 ", DL},{"g4 ", EN},{"h4 ", DN}, {"p", EN},{"d5 ", DN},{"h4 ", DN},{"d5 ", DN},{"d5 ", DN},{"d5 ", DN},{"d5 ", DN},{"e5 ", DN}, {"p", EN}, ,{"d5 ", DN},{"d5 ", DN},{"e5 ", DL},{"h4 ", EN},{"a4 ", DL},{"g4 ", EN},{"g4 ", DN}, {"p", EN},{"d5 ", DN},{"d5 ", DN},{"e5 ", DL},{"h4 ", EN},{"a4 ", DL},{"g4 ", EN},{"g4 ", DN},{"h4 ", DN},{"a4 ", DN},{"g4 ", DN},

 
  
  {"a4", DN},  {"a4",  DN},  {"g4",  EN},  {"a4",  DN},  {"h4",  DN},  {"p",  EN},  {"c5",  DN},  {"c5",  DN},  {"h4",  DN},  {"a4",  DN},  {"a4",  DN},  {"g4",  EN},  {"d5",  DN},  {"h4",  DN},  {"c5",  DN},  {"h4",  DN},  {"a4",  DN},  {"g4",  DN},  {"a4",  DN},  {"a4",  DN},  {"g4",  EN},  {"d5",  DN},  {"h4",  DN},  {"p",  EN},  {"d5",  DN},  {"e5",  DN},  {"h4",  DN},  {"p",  EN},  {"a4",  EN},  {"g4",  EN},  {"d5",  DN},  {"h4",  DN},  {"c5",  DN},  {"h4",  DN},  {"a4",  DN},  {"g4",  DN},  
  
 
  {"a4",  EN},  {"g4",  EN},  {"a4",  EN},  {"g4",  EN},  {"a4",  DN},  {"e4",  DN},  {"e5",  DN},  {"d5",  DN},  {"c5",  DN},  {"h4",  DN},  {"a4",  EN},  {"g4",  EN},  {"a4",  EN},  {"g4",  EN},  {"h4",  GN},  {"g4",  GN},  {"p",  EN},  {"d5",  DN},  {"c5",  DN},  {"h4",  DN},  {"h4",  DN},  {"a4",  EN},  {"g4",  EN},  {"a4",  DN},  {"g4",  DN},  {"c5",  DN},  {"h4",  DN},  {"a4",  DN},  {"g4",  DN},

};

struct CNote sun[] = {
 
{"g4", CN}, {"d4", DN}, {"f4", DN},  {"g4", CN},  {"f4", DN},  {"g4", DN},  {"h4b", CN},  {"d5", CN},  {"h4b", DN},  {"g4", DN},  {"p", DN},  


{"h4b", DN},  {"g4", CN},  {"h4b", DN},  {"d5", DN},  {"d5", CN},  {"f5", DN},  {"d5", CN},  {"h4b", CN},  {"g4", DN},  {"g4", DN},  {"p", DN},  
 
 
{"d5", DL},  {"d5", EN},  {"d5", DN},  {"f5", DN},  {"d5", DN},  {"d5", CN},  {"h4b", DN},  {"g4", DL},  {"f4", EN},  {"g4", DN},  {"d5", DN},  {"h4b", DN},  {"g4", DN},  {"p", DN},  

 
 
{"g4", DN},  {"f4", D},  {"g4", DN},  {"h4b", DN},  {"d5", CN},  {"h4b", CN},  {"g4", DN},  {"e4b", CN},  {"g4", DN},  {"g4b", CN},  {"p", DN},  

 
  
{"d5", DN},  {"d5", EN},  {"d5", DN},  {"d5", DN},  {"d5", DN},  {"f5", DN},  {"d5", DN},  {"h4b", DN},  {"g4", DL},  {"f4", EN},  {"g4", DN},  {"d5", DN},  {"h4b", DN},  {"d5", DN},  {"h4b", DN},  {"g4", DN},  

  
{"g4", DL},  {"f4", EN},  {"g4", DN},  {"h4b", DN},  {"d5", CN},  {"h4b", CN},  {"g4", DN},  {"f4", CN},  {"g4", DN},  {"h4b", CN},  {"d4", DN},  {"f4", DN},  {"g4", DN}  

 
  
{"h4b", CN},  {"g4", CN},  {"h4b", CN},  {"g4", CN},  {"h4b", DN},  {"g4", DN},  {"h4b", DN},  {"f5", DN},  {"d5", DN},  {"d5", DN},  {"p", DN},  {"h4b", CN},  {"g4", DN},  {"h4b", DN},  {"d5", DN},  {"d5", CN},  {"f5", DN},  {"d5", CN},  {"h4b", CN},  {"g4", DN},  {"g4", DN},  {"p", CN},  

 
  
{"g4", CN},  {"d4", DN},  {"f4", DN},  {"g4", DN},  {"g4", DN},  {"f4", DN},  {"g4", DN},  {"h4b", DN},  {"h4b", DN},  {"d5", CN},  {"h4b", DN},  {"g4", DN},  {"p", DN},  {"g4", DL},  {"f4", EN},  {"g4", DN},  {"h4b",  DN},  {"d5", CN},  {"h4b", CN},  {"g4", DL},  {"f4", EN},  {"g4", DN},  {"h4b", DN},  {"d5", CN},  {"h4b", CN},  


 
{"d5", DL},  {"d5", EN},  {"d5", DN},  {"f5", DN},  {"d5", DN},  {"d5", CN},  {"h4b", DN},  {"g4", DL},  {"f4", EN},  {"g4", DN},  {"d5", DN},  {"h4b", DN},  {"g4", DN},  {"p", DN},  {"g4", DN},  {"f4", DN},  {"g4", DN},  {"h4b", DN},  {"d5", CN},  {"h4b", CN},  {"g4", DN},  {"e4b", CN},  {"g4", DN},  {"h4b", CN},  {"p", DN},  

 
 
{"d5", DL},  {"d5", EN},  {"d5", DN},  {"f5", DN},  {"d5", DL},  {"d5", EN},  {"d5", DN},  {"f5", DN},  {"d5", EN},  {"f5", EN},  {"h4b", CN},  {"g4", DN},  {"f5", CN},  {"g5", DN},  {"f5", DN},  {"d5", BN},  
};


struct CNote wide[] = {
 

  {"c5", DN },{"c5 ", EN },{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"h4b ", DN },{"g4 ", DN },{"h4b ", DN },{"c5 ", DN }, 


  {"c5", DN },{"c5 ", EN },{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"d5 ", DN },{"d5 ", EN },{"d5 ", DN },{"d5 ", EN },{"d5 ", DN }, 
 
  
  {"c5", DN },{"c5 ", EN },{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"h4b ", DN },{"g4 ", DN },{"h4b ", DN },{"c5 ", DN }, 
 
 
  {"c5", DN },{"c5 ", EN },{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"d5 ", DN },{"d5 ", EN },{"d5 ", DN },{"d5 ", EN },{"d5 ", DN }, 
 
 
  {"c5", DN },{"c5 ", EN },{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"h4b ", DN },{"g4 ", DN },{"h4b ", DN },{"c5 ", DN }, 
 
{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"c5 ", EN },{"c5 ", DN },{"d5 ", DN },{"d5 ", EN },{"d5 ", DN },{"d5 ", EN },{"d5 ", DN },   
};


int Note [] = { 27,   31, 
                33,   37,   41,   44,   49,   55,   62,   
                65,   73,   82,   87,   98,   110,  123,
                131,  147,  165,  175,  196,  220,  247,
                262,  294,  330,  349,  392,  440,  494,
                523,  587,  659,  698,  784,  880,  988,
                1047, 1175, 1319, 1397, 1568, 1760, 1976,
                2093, 2349, 2637, 2794, 3136, 3520, 3951,
                4186, 4699 };

int Melodie [72][2] =  { {24, 10}, {24, 10}, {25, 20}, {24, 20}, {99, 20}, {25, 10}, 
                         {24, 10}, {25, 20}, {27, 20}, {99, 20}, {28, 10}, {28, 10}, 
                         {29, 20}, {28, 20}, {25, 10}, {25, 10}, {25, 10}, {24, 10},
                         {25, 10}, {27, 40}, {99, 20}, {99, 20}, {24, 10}, {24, 10}, 
                         {25, 20}, {24, 20}, {25, 10}, {27, 10}, {27, 10}, {29, 10}, 
                         {29, 20}, {99, 20}, {28, 10}, {28, 10}, {28, 10}, {28, 10}, 
                         {29, 20}, {28, 20}, {25, 10}, {27, 10}, {27, 10}, {28, 10}, 
                         {27, 20}, {27, 10}, {28, 10}, {29, 60}, {29,  5}, {28,  5}, 
                         {27, 10}, {28, 60}, {29, 10}, {28, 10}, {27, 40}, {99, 20}, 
                         {29, 10}, {31, 10}, {32, 60}, {32, 10}, {31, 10}, {31, 10}, 
                         {29, 50}, {29,  5}, {28,  5}, {27, 10}, {29, 50}, {29,  5}, 
                         {29,  5}, {28, 5},  {27, 60}, {27, 10}, {28, 10}, {27, 40}
                      };

#endif //UTIL_PLAKAT
