# LIBS=
# CFLAGS=${shell pkg-config --cflags ${LIBS}}
# LDFLAGS=${shell pkg-config --libs ${LIBS}}

RAWSRC=main.c
SRC=${addprefix ./src/, ${RAWSRC}}

OBJ=${SRC:.c=.o}


.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $< -o $@

main: ${OBJ}
	@echo LD $@ 
	@${CC} -o $@ ${OBJ} ${LDFLAGS}


