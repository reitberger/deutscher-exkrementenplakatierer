# Docs on /flake.nix
flakes are a tool within the nix package manager that allow hermetic builds and easy dependency resolution for projects. This project utilizes flakes with the devShells.x86_64-linux.default output which builds a Derivation which itself says that it depends on `gnumake` (for convenient compiling), `arduino` and `screen` to interact with the serial console?
basically I've just copied the configuration from the [arduino guide](https://nixos.wiki/wiki/Arduino). Runnig this may take some time and space to download everything but it is what it is.
tldr; 
- install the [nix package manager](https://nixos.org/download.html)
- run `nix develop`
- boom you should be dropped into a shell with all necessary packages (yet!)
