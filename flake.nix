# for more information refer to the ./doc directory
{
  description = "proj3";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05-small";
  };
  outputs = {self, nixpkgs, ...}:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
      {
        devShells.${system}.default = pkgs.stdenv.mkDerivation {
          name = "proj3";
          nativeBuildInputs = with pkgs; [pkg-config];
          buildInputs = with pkgs; [arduino arduino-cli gnumake screen];
        };
      };
}
